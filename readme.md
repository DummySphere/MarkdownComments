
# MarkdownComments

A Visual Studio extension that parse Markdown in code comments.

![screenshot](Resources/screenshot.png)

* [Supported syntax](#supported-syntax)
* [Next steps](#next-steps)
* [History](#history)
* [License](#license)

## Supported syntax:

* Emphasis, aka italics, with \*asterisks\* or \_underscores\_.
* Strong emphasis, aka bold, with \*\*asterisks\*\* or \_\_underscores\_\_.
* Combined emphasis with \*\*asterisks and \_underscores\_\*\*.
* Strikethrough uses two tildes. \~\~Scratch this.\~\~
* \# Header 1 to \#\#\#\#\#\# Header 6
* \!\[image alt text\]\(http://imageurl/image.png "optional title"\)
* \!\[image alt text\]\(image_path_relative_to_code_file.png "optional title"\)

## Next steps

* Compatibility with VS2012
* Visual Studio variables like $(ProjectDir) in image paths
* Markdown links
* Option to distinguish // vs. /// ?
* Drag & Drop images

Bug reports and suggestions are welcome ... :)

## History

See [changelog](changelog.txt).

## License

Copyright (c) 2015-2016 Julien Duminil  
This project is released under the [MIT License](http://opensource.org/licenses/MIT).
